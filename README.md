# StackTool
OpenStack utils

# Usage
Example of the client docker container startup script.

```
#!/bin/bash
 
#For having dynamic access to more projects, it's possible to have rc.sh files in separated dirs
RCSH_PATH=$1
 
#if nothing's provided, use rc.sh in the same dir as the script
if [ -z "$RCSH_PATH" ]; then
  RCSH_PATH="$(dirname "$(realpath "$0")")"
fi
 
#rc.sh will envoke its password prompt
source $RCSH_PATH/rc.sh 
 
#Set the project name in the bash based on the project dir if provided
PROJECT_DIR=$(echo $RCSH_PATH | awk -F"/" '{print $NF}')

#The PROJECT_DIR ENV var is used for display in the prompt, You can set it to $OS_PROJECT_ID if needed
docker run --rm -ti \
  -e OS_AUTH_URL=$OS_AUTH_URL \
  -e OS_IDENTITY_API_VERSION="3" \
  -e OS_VOLUME_API_VERSION="2" \
  -e OS_PROJECT_DOMAIN_NAME=$OS_PROJECT_DOMAIN_NAME \
  -e OS_PROJECT_ID=$OS_PROJECT_ID \
  -e OS_TENANT_ID=$OS_PROJECT_ID \
  -e OS_REGION_NAME=$OS_REGION_NAME \
  -e OS_USER_DOMAIN_NAME=$OS_PROJECT_DOMAIN_NAME \
  -e OS_USERNAME=$OS_USERNAME \
  -e OS_ESCAPED_USERNAME=$OS_ESCAPED_USERNAME \
  -e OS_PASSWORD=$OS_PASSWORD \
  -e PROJECT_DIR=$PROJECT_DIR \
  mrazu/stacktool:latest
```
