FROM ubuntu:18.04

RUN apt-get update && apt-get -y install curl python-pip python-dev python3-dev git libxml2-dev libxslt1-dev python-openssl python3-openssl python-pyasn1 libffi-dev libssl-dev build-essential bash-completion vim
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl > /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl

RUN pip install -UI pip
RUN pip install -UI pbr setuptools pytz
RUN pip install -UI git+https://github.com/openstack/python-openstackclient \
                    git+https://github.com/openstack/python-cinderclient \
                    git+https://github.com/openstack/python-glanceclient \
                    git+https://github.com/openstack/python-neutronclient \
                    git+https://github.com/openstack/python-novaclient \
                    git+https://github.com/openstack/python-heatclient \
                    git+https://github.com/openstack/python-magnumclient
RUN openstack complete | tee /etc/bash_completion.d/osc.bash_completion > /dev/null
RUN printf 'source /etc/profile \n__kube_prompt() { \n  echo "($(kubectl config current-context > /dev/null 2>&1))" \n} \nKUBE_PROMPT="\$(__kube_prompt)" \nPS1="$PS1$PROJECT_DIR${KUBE_PROMPT}\\\$ "' >> /root/.bashrc
